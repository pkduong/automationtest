+ This automated browser test 3 basic test case of JIRA:

	TC01• New issues can be created
	TC02• Existing issues can be updated
	TC03• Existing issues can be found via JIRA’s search

	The script is written with Selenium WebDriver using PageObject pattern, language Java. The project has included TestNG plug-in for supporting test report and test assertion.

+ Version detail:
	- selenium-server-standalone-2.42.2
	- Java jdk 1.6
	- Plugin: TestNG 6.8.6.20141201_2240
	- IDE: eclipse-standard-kepler-SR2-win32-x86_64

+ How to run the script
	Precondition:
		- Firefox version 30 (or earlier) installed and ready for use.
		- Eclipse IDE install and ready for use.
		- Copy TestNG plugin into folder \eclipse\plugins\ 
	Steps:
		1. Import project "JiraTestByDuongPK" (extracted from zip) into Eclipse IDE.
		2. Make sure it's compiled successfully
		3. Right click on project > Run As > TestNG
		> Wait for a while.. Firefox browser will be launched, Jira login page display, and the test has begun...
	
	Notes:
		- The script using my account AtlasID to login, if you want to change yours, please update it at RunTest class.
		
+ Project hierarchy and description:
	- package com.atlas.PageObject.pages;
		-- PageBase.java
		. This class is based for pages that will be extended by page unit
		
	- package com.atlas.PageObject.tests;
		-- TestBase.java
		. This class is test base that will be extended by run test class
		
	- package com.atlas.PageObject.Jira.pages;
		-- HomePage.java
		-- IssuePage.java
		-- LoginPage.java
		-- SearchIssuePage.java
		. These classes contain page unit of JIRA app, contain web elements and actions.
		
	- package com.atlas.PageObject.Jira.tests;
		--RunTest.java
		. This class contain series of tests, the test run for 3 test case is triggered here, using annotation @Test at class "SanityTest1"
		
+ Issue found during test:
	- Intermittent: Verify issue updated (TC02) sometime get failed since JIRA does not update the summary accordingly.
	Steps:
		1. create issue with summary: "Test 1" 
		> issue created OK
		2. go to issue page, and click Edit button 
		> edit page popup: OK
		3. change summary issue from "Test 1" to "Test 1b", click Update button
		> the popup closed: OK, but the summary of issue still in "Test 1"
		4. click edit button again
		> see the summary is "Test 1b"
	Conclusion: this bug is intermittent, I see this bug only twice while running my test over 20 times

+ Limitation:
	- Support Firefox only, best version 30 or ealier.

+ Known issue:
	- You might get issue below when running with Firefox 33 or later.
	org.openqa.selenium.firefox.NotConnectedException: Unable to connect to host 127.0.0.1 on port 7055 after 45000 ms. Firefox console output:
	ded provider scope for resource://gre/modules/LightweightThemeManager.jsm: ["LightweightThemeManager"]


