package com.atlas.PageObject.pages;


import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author DuongPK
 * BASEPAGE: This class is based for pages that will be extended by page unit
 */
public class PageBase {

	protected String URL;
	protected String pageTitle;
	protected WebDriver driver;

	public PageBase(WebDriver driver, String pageTitle) {
		this.driver = driver;
		this.pageTitle = pageTitle;
	}

	/**
	 * Get element by xpath stringLocator
	 * 
	 * @param xpathStringLocator
	 * @return WebElement
	 * @throws Exception
	 */
	public void open() {
		driver.get(this.URL);
		driver.manage().window().maximize();
	}

	/**
	 * Get element by xpath stringLocator
	 * 
	 * @param xpathStringLocator
	 * @return WebElement
	 * @throws Exception
	 */
	public WebElement getElement(String locator) {
		if (isElementExist(locator)) {
			return driver.findElement(By.xpath(locator));
		} else {
			return null;
		}

	}

	/**
	 * Get element by object By
	 * 
	 * @param xpath
	 * @return WebElement
	 * @throws Exception
	 */
	public WebElement getElement(By b) {
		if (isElementExist(b)) {
			return getWebElement(b);
		} else {
			return null;
		}

	}

	/**
	 * Check element exist by string xpath
	 * 
	 * @param stringLocator
	 * @return
	 * @throws Exception
	 */
	public boolean isElementExist(String locator) {
		try {
			driver.findElement(By.xpath(locator));
			System.out.println("Element " + locator + " present");
			return true;

		} catch (NoSuchElementException e) {
			System.out.println("Element " + locator + "absent");
			return false;
		}
	}

	/**
	 * Check element exist by object By
	 * 
	 * @param b
	 * @return
	 */
	public boolean isElementExist(By b) {
		List<WebElement> webElements = driver.findElements(b);
		if (webElements.size() >= 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check element exist and display on screen, using object By
	 * 
	 * @param b
	 * @return
	 */
	public boolean isElementExistAndDisplay(By b) {
		boolean result = false;
		if (isElementExist(b)) {
			result = getWebElement(b).isDisplayed();
		}
		return result;
	}
	
	/**
	 * Check element exist and display on screen, using string location
	 * 
	 * @param stringLocator
	 * @return
	 */
	public boolean isElementExistAndDisplay(String locator) {
		boolean result = false;
		if (isElementExist(locator)) {
			result = getWebElement(locator).isDisplayed();
		}
		return result;
	}

	public WebElement getWebElement(By b) {
		return driver.findElement(b);
	}

	public WebElement getWebElement(String locator) {
		return driver.findElement(By.xpath(locator));
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

}
