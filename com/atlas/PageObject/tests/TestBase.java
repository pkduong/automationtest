package com.atlas.PageObject.tests;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * @author DuongPK
 * TESTPAGE: This class is test base that will be extended by run test class
 */
public class TestBase {

	public WebDriver driver;

	@BeforeClass
	public void beforeSuite() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	@AfterClass
	public void afterSuite() {
		//driver.quit();
	}

	public String getCurrentURL() {
		return driver.getCurrentUrl();
	}

	public String getCurrentTitle() {
		return driver.getTitle();
	}

	//@Test
	public void testBaseRunTest() {
		System.out.println("Just for test... this class run from TestBase class");
	}

	

}
