package com.atlas.PageObject.Jira.tests;

import org.testng.AssertJUnit;

import org.testng.annotations.Test;

import com.atlas.PageObject.tests.TestBase;
import com.atlas.PageObject.Jira.pages.HomePage;
import com.atlas.PageObject.Jira.pages.IssuePage;
import com.atlas.PageObject.Jira.pages.LoginPage;
import com.atlas.PageObject.Jira.pages.SearchIssuePage;

/**
 * @author DuongPK
 * This class contain series of tests
 */
public class RunTest extends TestBase{
	
	private String username="pkduong@gmail.com";
	private String password="duongthu";
	private String issueSummary="Harry Potter and the Fire Goblet";
	private String issueSummary2="The Hobbit";
	
	public RunTest() {
		// TODO Auto-generated constructor stub
	}
	
	@Test
	/**
	 * This test SanityTest1 is to verify 3 basic test case.
	 * 1. Verify that new issue can be created.
	 * 2. Verify that existing issue can be updated.
	 * 3. Verify that existing issue can be found via JIRA's search.
	 */
	public void SanityTest1() throws InterruptedException{
		
	//The very first steps: Login
		LoginPage loginPage = new LoginPage(driver);
		HomePage homePage = loginPage.doLoginAs(username, password);
		
	// TC01: Verify that new issue can be created.
		
		homePage.clickCreateIssue();
		homePage.CreateIssueWithSummary(issueSummary);
		
		//Validation point of TC01
		try{
			AssertJUnit.assertTrue(homePage.checkIssueCreatedInRecentList(issueSummary));
		}catch (AssertionError e){
			System.out.println(e.getMessage());
		}
		
	// TC02: Verify that existing issue can be updated.
		IssuePage issuePage = homePage.goToIssuePageByKey(HomePage.dataIssueKey);
		
		issuePage.updateIssueSummary(issueSummary2);
		
		issuePage = homePage.goToIssuePageByKey(HomePage.dataIssueKey);
		
		//Validation point of TC02
		try{
			AssertJUnit.assertTrue(issuePage.checkIssueTitleAfterChange(issueSummary2));
		}catch (AssertionError e){
			System.out.println(e.getMessage());
		}
		
	// TC03: Verify that existing issue can be found via JIRA's search.
		SearchIssuePage searchIssuePage = homePage.goToSearchIssuePage();
		
		searchIssuePage.doSearchIssueBySummary(issueSummary2);
		
		Thread.sleep(4000);
		
		//Validation point of TC03
		try{
			AssertJUnit.assertTrue(searchIssuePage.checkIssueCreatedFoundInSearchResultList(issueSummary2));
		}catch (AssertionError e){
			System.out.println(e.getMessage());
		}
	}
	
	
}
	
