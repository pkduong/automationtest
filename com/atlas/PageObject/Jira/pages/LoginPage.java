package com.atlas.PageObject.Jira.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.atlas.PageObject.pages.PageBase;

/**
 * @author DuongPK
 * LOGIN-PAGE: This class extend from PageBase having elements/actions login-page
 */
public class LoginPage extends PageBase {

	private static String pageTitle="Sign in to continue";
	
	private String locUserName = "//input[@id='username']";
	private String locPassword = "//input[@id='password']";
	private String locSubmit = "//input[@id='login-submit']";

	public LoginPage(WebDriver driver) {
		super(driver,pageTitle);
		super.URL = "https://id.atlassian.com/login?continue=https%3A%2F%2Fjira.atlassian.com%2Fsecure%2FDashboard.jspa&application=jac";
		super.open();
	}
	
	private WebElement getUsernameTb(){
		return driver.findElement(By.xpath(locUserName));
	}
	
	private WebElement getPasswordTb(){
		return driver.findElement(By.xpath(locPassword));
	}
	
	private WebElement getSignInBtn(){
		return driver.findElement(By.xpath(locSubmit));
	}

	/**
	 * Function to login with user name and password
	 * 
	 * @param username
	 * @param password
	 */
	private void LoginAs(String username, String password){
		getUsernameTb().clear();
		getUsernameTb().sendKeys(username);
		
		getPasswordTb().clear();
		getPasswordTb().sendKeys(password);
		
		getSignInBtn().submit();
	}
	
	/**
	 * Do login with user name and password
	 * 
	 * @param username
	 * @param password
	 * @return HomePage
	 */
	public HomePage doLoginAs(String username, String password){
		System.out.println("Login with UN/PW:" + username + "/" + password);
		
		LoginAs(username,password);
		return new HomePage(driver);
	}

}
