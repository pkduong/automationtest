package com.atlas.PageObject.Jira.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.atlas.PageObject.pages.PageBase;

/**
 * @author DuongPK
 * ISSUE-PAGE: This class extend from PageBase having elements/actions of issue-page detail
 */
public class IssuePage extends PageBase {
	private static String pageTitle = " - Atlassian JIRA";
	
	public IssuePage(WebDriver driver, String dataKey) {
		super(driver,pageTitle);
		super.URL = "https://jira.atlassian.com/browse/" + dataKey;
		super.open();
	}
	
	private String locEditBtn="//a[@id='edit-issue']";
	private String locSummaryTxt="//input[@id='summary']";
	private String locUpdateBtn="//input[@id='edit-issue-submit']";
	private String locH1Summary="//h1[@id='summary-val']";
	
	private WebElement getEditBtn(){
		return driver.findElement(By.xpath(locEditBtn));
	}
	
	private WebElement getSummaryTxt(){
		return driver.findElement(By.xpath(locSummaryTxt));
	}
	
	private WebElement getUpdateBtn(){
		return driver.findElement(By.xpath(locUpdateBtn));
	}
	
	private WebElement getH1Summary(){
		return driver.findElement(By.xpath(locH1Summary));
	}
	
	/**
	 * Update issue Summary
	 * 
	 * @param issueSummary
	 */
	public void updateIssueSummary(String summary_new){
		getEditBtn().click();
		
		getSummaryTxt().clear();
		getSummaryTxt().sendKeys(summary_new);
		
		getUpdateBtn().click();
	}
	
	/**
	 * Check issue summary after edit 
	 * 
	 * @param issueSummary
	 * @return boolean
	 */
	public boolean checkIssueTitleAfterChange(String summary_change){
		boolean change=false;
		
		if (getH1Summary().getText().compareTo(summary_change) == 0) {
			change = true;
		}
		
		System.out.println("TC02: Verify that existing issue can be updated.");
		System.out.println("getH1Summary().getText() = " + getH1Summary().getText());
		
		return change;
	}
	
}
