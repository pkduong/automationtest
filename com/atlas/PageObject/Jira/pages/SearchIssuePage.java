package com.atlas.PageObject.Jira.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.atlas.PageObject.pages.PageBase;

/**
 * @author DuongPK
 * SEARCH-PAGE: This class extend from PageBase having elements/actions search-issue-page
 */
public class SearchIssuePage extends PageBase {
	
	private static String pageTitle="";
	
	private String locSearchTxt = "//input[@id='searcher-query']";
	private String locSearchBtn = "//button[@original-title='Search for issues']";
	
	private String locSearchResult = "//span[@class='issue-link-summary']";
	
	public SearchIssuePage(WebDriver driver) {
		super(driver,pageTitle);
		super.URL = "https://jira.atlassian.com/issues/?jql=";
		super.open();
	}
	
	private WebElement getSearchTb(){
		return driver.findElement(By.xpath(locSearchTxt));
	}
	
	private WebElement getSearchBtn(){
		return driver.findElement(By.xpath(locSearchBtn));
	}
	
	private WebElement getSearchResult(){
		return driver.findElement(By.xpath(locSearchResult));
	}
	
	/**
	 * Do submit search issue by Summary
	 * 
	 * @param issueSummary
	 * @return boolean
	 */
	public void doSearchIssueBySummary(String summary_query){
		getSearchTb().clear();
		getSearchTb().sendKeys(summary_query);
		
		getSearchBtn().click();
	}
	
	/**
	 * Check issue existing in return search result list
	 * 
	 * @param issueSummary
	 * @return boolean
	 */	
	public boolean checkIssueCreatedFoundInSearchResultList(String summary_query){
		boolean found=false;
		
		if (super.isElementExist(locSearchResult)) {
			WebElement issueSummarySpan =  driver.findElement(By.xpath(locSearchResult));
			if (issueSummarySpan.getText().compareTo(summary_query) == 0){
				found = true;
			}
			System.out.println("TC03: Verify that existing issue can be found via JIRA's search.");
			System.out.println("issueSummarySpan.getText() = " + issueSummarySpan.getText());
		}
		
		return found;
	}
	
	
}
