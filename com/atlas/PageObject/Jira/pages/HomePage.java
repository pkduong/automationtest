package com.atlas.PageObject.Jira.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.atlas.PageObject.pages.PageBase;

/**
 * @author DuongPK
 * HOME-PAGE: This class extend from PageBase having elements/actions of home-page and create-issue-page
 */
public class HomePage extends PageBase {
	private static String pageTitle="A Test Project - Atlassian JIRA";
	
	private String locLogin="//a[@id='user-options']";
	private String locCreateIssue="//a[@id='create_link']";
	private String locSearchIssue="//a[@id='issues_new_search_link_lnk']";
	private String locIssueMenu = "//a[@id='find_link']";
	
	public static String dataIssueKey="";
	
	public HomePage(WebDriver driver) {
		super(driver,pageTitle);
		super.URL = "https://jira.atlassian.com/browse/TST/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel";
		super.open();
	}
	
	private WebElement getLoginLnk(){
		return driver.findElement(By.xpath(locLogin));
	}
	
	private WebElement getCreateBtn(){
		return driver.findElement(By.xpath(locCreateIssue));
	}
	
	private WebElement getSearchIssueLnk(){
		return driver.findElement(By.xpath(locSearchIssue));
	}
	
	private WebElement getIssueMenuLnk(){
		return driver.findElement(By.xpath(locIssueMenu));
	}
	
	
	public void clickCreateIssue(){
		getCreateBtn().click();
	}
	
	public void clickLogin(){
		getLoginLnk().click();
	}
	
	public void clickSearchIssue(){
		getSearchIssueLnk().click();
	}
	
	// Create issue pop up dialog
	
	private String locSummary="//input[@id='summary']";
	private String locSubmit="//input[@id='create-issue-submit']";
	
	private WebElement getSummaryTb(){
		return driver.findElement(By.xpath(locSummary));
	}
	
	private WebElement getSubmitBtn(){
		return driver.findElement(By.xpath(locSubmit));
	}
	
	/**
	 * Create new issue with Summary
	 * 
	 * @param issueSummary
	 */
	public void CreateIssueWithSummary(String summary){
		System.out.println("Create issue with summary: " + summary);
		
		getSummaryTb().clear();
		getSummaryTb().sendKeys(summary);
		getSubmitBtn().click();
	}
	
	/**
	 * Check issue existing on RecentList right after created
	 * 
	 * @param issueSummary
	 * @return boolean
	 */
	public boolean checkIssueCreatedInRecentList(String summary){
		boolean found=false;

		String locIssueTitle="//a[contains(@title,'" + summary + "')]";
		
		getIssueMenuLnk().click();
		
		System.out.println("TC01: Verify that new issue can be created.");
		
		if (super.isElementExist(locIssueTitle)) {
			found = true;
			WebElement issueTitleRecent = driver.findElement(By.xpath(locIssueTitle));
			this.dataIssueKey = issueTitleRecent.getAttribute("data-issue-key");
			
			System.out.println("dataIssueKey=" + dataIssueKey);
			System.out.println("dataIssueTitle=" + issueTitleRecent.getAttribute("title"));
			
			//issueTitleRecent.click();
		}
		
		return found;
	}
	
	public IssuePage goToIssuePageByKey(String dataKey){
		return new IssuePage(driver,dataKey); 
	}
	
	public SearchIssuePage goToSearchIssuePage(){
		return new SearchIssuePage(driver); 
	}

}
